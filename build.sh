#!/bin/bash
# BUILD REQS:: pacman -Sy go git upx gcc pkgconf
function build() {
    go get -v .
    [[ -f gtk-pe ]] && rm gtk-pe
    go build -v -ldflags="-s -w" -o gtk-pe.b .
    upx -v -o gtk-pe gtk-pe.b
    rm gtk-pe.b
}
function installFiles() {
    [[ ! -f gtk-pe ]] && build
    cp gtk-pe.desktop /home/alarm/.local/share/applications/
    sudo "cp gtk-pe.png /usr/share/icons/ ; cp gtk-pe /usr/bin/"
}
function update() {
    git reset --hard origin/master
    git clean -fxd
    git pull
    build
    sudo "cp gtk-pe /usr/bin"
}
function sudo() {
    su root -c "${1}"
}
function remove() {
    sudo "rm /usr/share/icons/gtk-pe.png /usr/bin/gtk-pe /home/alarm/.local/share/applications/gtk-pe.desktop"
    file /usr/share/icons/gtk-pe.png
    file /usr/bin/gtk-pe
    file /home/alarm/.local/share/applications/gtk-pe.desktop
}
case ${1} in
    "install")installFiles;;
    "update")update;;
    "remove")remove;;
    *)build;;
esac
